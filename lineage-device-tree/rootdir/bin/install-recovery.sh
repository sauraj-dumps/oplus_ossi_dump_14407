#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:ffa857de6395dea7e5fe0aceca85b0f7a29bcdef; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:70b2026137c651ead85985aba3d9ccc193ab1c92 \
          --target EMMC:/dev/block/by-name/recovery:134217728:ffa857de6395dea7e5fe0aceca85b0f7a29bcdef && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
